class Player {
	constructor(element) {
		this.mouseDown = false

		/* Get elements */
		this.video = element.querySelector('.viewer')
		this.progress = element.querySelector('.progress')
		this.progressBar = element.querySelector('.progress__filled')
		this.toggle = element.querySelector('.toggle')
		this.skipButtons = element.querySelectorAll('[data-skip]')
		this.ranges = element.querySelectorAll('.player__slider')

		/* Event listeners */
		this.video.addEventListener('click', this.togglePlay.bind(this))
		this.toggle.addEventListener('click', this.togglePlay.bind(this))
		this.video.addEventListener('play', this.updateButton.bind(this))
		this.video.addEventListener('pause', this.updateButton.bind(this))
		this.video.addEventListener('timeupdate', this.handleProgress.bind(this))

		this.skipButtons.forEach(button => button.addEventListener('click', this.skip.bind(this)))

		this.ranges.forEach(range => range.addEventListener('change', this.handleRangeUpdate.bind(this)))
		this.ranges.forEach(range => range.addEventListener('mousemove', this.handleRangeUpdate.bind(this)))

		this.progress.addEventListener('click', this.scrub.bind(this))
		this.progress.addEventListener('mousemove', (e) => this.mouseDown && this.scrub(e))
		this.progress.addEventListener('mousedown', () => this.mouseDown = true)
		this.progress.addEventListener('mouseup', () => this.mouseDown = false)

	}

	togglePlay() {
		const method = this.video.paused ? 'play' : 'pause'
		this.video[method]()
		this.updateButton()
	}

	updateButton() {
		this.toggle.textContent = this.video.paused ? '►' : '❚ ❚'
	}

	skip(e) {
		this.video.currentTime += parseFloat(e.target.dataset.skip)
	}

	handleRangeUpdate(e) {
		this.video[e.target.name] = e.target.value
	}

	handleProgress() {
		const percent = (this.video.currentTime / this.video.duration) * 100
		this.progressBar.style.flexBasis = `${percent}%`
	}

	scrub(e) {
		const scrubTime = (e.offsetX / this.progress.offsetWidth) * this.video.duration
		this.video.currentTime = scrubTime
	}
}

const player = new Player(document.querySelector('.player'))
