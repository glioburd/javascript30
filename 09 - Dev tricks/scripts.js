const dogs = [{ name: 'Snickers', age: 2 }, { name: 'hugo', age: 8 }]

function makeGreen() {
	const p = document.querySelector('p')
	p.style.color = '#BADA55'
	p.style.fontSize = '50px'
}

// Regular

// Interpolated

// Styled

console.log('%c Coucou je suis gentil', 'font-size:50px; color:red;')

// warning!

console.warn('WARNING !')

// Error :|

console.error('ERROR !')

// Info

console.info('Fun fact : blablabla')

// Testing

const p = document.querySelector('p')
console.assert(p.classList.contains('ouch'), 'That is wrong!')

// clearing

//console.clear()

// Viewing DOM Elements

console.log(p)
console.dir(p)

// Grouping together
console.clear()
dogs.forEach(dog => {
	console.group(`${dog.name}`) // console.groupCollapsed(`${dog.name}`) pour groupes fermés
	console.log(`This is ${dog.name}`)
	console.log(`${dog.name} is ${dog.age} years old`)
	console.log(`${dog.name} is ${dog.age * 7} dogs years old`)
	console.groupEnd(`${dog.name}`)

})

// counting

console.count('Steve')
console.count('Steve')
console.count('Steve')
console.count('Steve')
console.count('Steve')
console.count('Wes')
console.count('Wes')
console.count('Steve')
console.count('Steve')

// timing
console.time('fetching datas')
fetch('https://api.github.com/users/wesbos')
	.then(data => data.json())
	.then(data => {
		console.timeEnd('fetching datas')
		console.log(data)
	})