function playSound(e) {
	const audio = document.querySelector(`audio[data-key="${e.keyCode}"]`)
	const key = document.querySelector(`div[data-key="${e.keyCode}"]`)

	if (!audio || !key) return
	key.classList.add('playing')
	audio.currentTime = 0
	audio.play()
}

function removePlaying(e) {
	e.target.classList.remove('playing')
}

function switchLayout(e) {
	let keyToFind
	let keyToChange
	let charToChange

	if (e.target.checked) {
		keyToFind = '65'
		keyToChange = '81'
		charToChange = 'Q'
	} else {
		keyToFind = '81'
		keyToChange = '65'
		charToChange = 'A'
	}

	const aKey = document.querySelector(`div[data-key="${keyToFind}"]`)
	const aKeyKbd = aKey.getElementsByTagName('kbd')
	const audio = document.querySelector(`audio[data-key="${keyToFind}"]`)

	aKey.setAttribute('data-key', keyToChange)
	aKeyKbd[0].innerHTML = charToChange
	audio.setAttribute('data-key', keyToChange)
}

const keys = Array.from(document.querySelectorAll('.key'))

keys.forEach(key => {
	key.addEventListener('transitionend', removePlaying)
})

window.addEventListener('keydown', playSound)

const azerty = document.querySelector('#azerty')
azerty.addEventListener('change', switchLayout)
