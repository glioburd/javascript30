function numberWithCommas(x) {
	return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

function addSuggestion(match) {
	console.log(match)
	const li = document.createElement('li')
	const newSuggestion = document.createTextNode('Hi there and greetings!')
	li.appendChild(newSuggestion)
	suggestions.appendChild(li)
}

function findMatches(wordToMatch, cities) {
	const regex = new RegExp(wordToMatch, 'gi')
	return cities.filter(place => {
		return place.city.match(regex) || place.state.match(regex)
	})
}

function displayMatches() {
	if (this.value) {
		const matchArray = findMatches(this.value, cities)
		const html = matchArray.map(place => {
			const regex = new RegExp(this.value, 'gi')
			const cityName = place.city.replace(regex, `<span class="hl">${this.value}</span>`);
			const stateName = place.state.replace(regex, `<span class="hl">${this.value}</span>`);
			return `
				<li>
					<span class="name">${cityName}, ${stateName}</span>
					<span class="population">${numberWithCommas(place.population)}</span>
				</li>
			`
		}).join('')
		suggestions.innerHTML = html
	} else {
		return suggestions.innerHTML = `
			<li>Filter for a city</li>
			<li>or a state</li>
		`
	}

}

const endpoint =
	'https://gist.githubusercontent.com/Miserlou/c5cd8364bf9b2420bb29/raw/2bf258763cdddd704f8ffd3ea9a3e81d25e2c6f6/cities.json'
const cities = []

// let response = await fetch(endpoint)
// let commits = await response.json()

fetch(endpoint)
	.then(response => response.json())
	.then(data => cities.push(...data))
	.catch(error => {
		data.answer = "Erreur ! Impossible d'accéder au fichier JSON : " + error
	})

const searchForm = document.querySelector('.search')
const suggestions = document.querySelector('.suggestions')

searchForm.addEventListener('change', displayMatches);
searchForm.addEventListener('keyup', displayMatches);
