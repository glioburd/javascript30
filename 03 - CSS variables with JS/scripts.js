const inputs = document.querySelectorAll('.picture-settings input')

function handleUpdate () {
	const suffix = this.dataset.sizing || ''
	document.documentElement.style.setProperty(`--${this.name}`, `${this.value}${suffix}`)
}

inputs.forEach(input => input.addEventListener('mousemove', handleUpdate))
inputs.forEach(input => input.addEventListener('change', handleUpdate))

