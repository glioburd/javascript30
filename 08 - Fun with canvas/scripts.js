class Drawer {
	/**
	 *
	 * @param {HTMLElement} canvas
	 * @param {Object} options
	 */
	constructor(canvas, options = {}) {
		this.canvas = canvas
		this.options = Object.assign(
			{},
			{
				strokeStyle: '#BADA55',
				lineJoin: 'round',
				lineCap: 'round'
			},
			options
		)
		this.contexte = this.canvas.getContext('2d')
		this.contexte.strokeStyle = this.options.strokeStyle
		this.contexte.lineJoin = this.options.lineJoin
		this.contexte.lineCap = this.options.lineCap
		this.contexte.lineWidth = 25;

		this.tool = 'rainbow'

		this.isDrawing = false
		this.inDrawZone = false
		this.lastX = 0
		this.lastY = 0

		// Evenements
		canvas.addEventListener('mousemove', (e) => this.draw(e))
		canvas.addEventListener('mousedown', (e) => this.mouseDown(e))
		canvas.addEventListener('mouseup', () => (this.isDrawing = false))
		canvas.addEventListener('mouseout', () => this.mouseOut())
	}

	draw(e) {
		if (!this.isDrawing) return
		this.contexte.beginPath()
		this.contexte.moveTo(this.lastX, this.lastY)
		this.contexte.lineTo(e.offsetX, e.offsetY)
		this.contexte.stroke()
		this.lastX = e.offsetX
		this.lastY = e.offsetY
	}

	mouseOut () {
		this.isDrawing = false
	}

	mouseDown(e) {
		this.isDrawing = true
		this.lastX = e.offsetX
		this.lastY = e.offsetY
	}
}

const drawZone = document.querySelector('.draw-zone')
const canvas = document.querySelector('#draw')

canvas.width = drawZone.offsetWidth
canvas.height = drawZone.offsetHeight

document.addEventListener('DOMContentLoaded', function() {
	const drawer = new Drawer(document.querySelector('#draw'))
})
