// ## Array Cardio Day 2

const people = [
	{ name: 'Wes', year: 1988 },
	{ name: 'Kait', year: 1986 },
	{ name: 'Irv', year: 1970 },
	{ name: 'Lux', year: 2015 }
]

const comments = [
	{ text: 'Love this!', id: 523423 },
	{ text: 'Super good', id: 823423 },
	{ text: 'You are the best', id: 2039842 },
	{ text: 'Ramen is my fav food ever', id: 123523 },
	{ text: 'Nice Nice Nice!', id: 542328 }
]

console.table(comments)

// Some and Every Checks
// Array.prototype.some() // is at least one person 19 or older?
const checkForUnderage = function(e) {
	const currentYear = new Date().getFullYear()
	return currentYear - e.year >= 19
}

const resultCheckForUnderage = people.some(checkForUnderage)
console.log('Is at least one person 19 or older?')
console.log(resultCheckForUnderage)

// Array.prototype.every() // is everyone 19 or older?

const isEveryoneAdult = people.every(checkForUnderage)
console.log('Is everyone 19 or older?')
console.log(isEveryoneAdult)

// Array.prototype.find()
// Find is like filter, but instead returns just the one you are looking for
// find the comment with the ID of 823423

const getCommentWithId = comments.find(e => e.id === 823423)
console.log('Find the comment with the ID of 823423')
console.log(getCommentWithId)

// Array.prototype.findIndex()
// Find the comment with this ID
// delete the comment with the ID of 823423

const index = comments.findIndex(e => e.id === 823423)
const newComments = [
	...comments.slice(0, index),
	...comments.slice(index + 1)
]

console.log('Find the comment 823423 and delete it')
console.table(newComments)