function handleCheck(e) {
	let inBetween = false
	if (e.shiftKey && this.checked) {
		checkboxes.forEach(checkBox => {
			console.log(checkBox)
			if (checkBox === this || checkBox === lastChecked) {
				inBetween = !inBetween
			}

			if (inBetween) {
				checkBox.checked = true
			}
		}) 
	}
	lastChecked = this

}

const checkboxes = document.querySelectorAll('.inbox input[type="checkbox"]')
let lastChecked

checkboxes.forEach(checkBox => checkBox.addEventListener('click', handleCheck))